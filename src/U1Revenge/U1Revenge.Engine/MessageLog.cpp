#include <string>
#include <sstream>

#include "MessageLog.h"

using std::string;
using std::stringstream;
using std::getline;

// private methods

void MessageLog::PushPrivate(string message)
{
    // message log can have up to 3 lines of text; 4th line is blank waiting for input

    lines[3] += message;

	for (int i = 0; i < 3; i++)
		lines[i] = lines[i + 1];

	lines[3] = "";
}

// public methods

MessageLog::MessageLog()
    : isDirty(true), lines(4)
{
    for (int i = 0; i < 4; i++)
        lines[i] = "";
}

MessageLog::~MessageLog()
{
}

void MessageLog::WriteLine(const char * message)
{
    // split into lines

    stringstream ss(message);

    string line;

    while (getline(ss, line, '\n'))
        PushPrivate(line);

	isDirty = true; // signal to view that this needs re-rendering
}

void MessageLog::Write(const char * message)
{
	lines[3] += message;

	isDirty = true; // signal to view that this needs re-rendering
}

vector<string>* MessageLog::GetMessages()
{
	return &lines;
}

bool MessageLog::GetAndRemoveDirty()
{
	bool wasDirty = isDirty;
	isDirty = false;
	return wasDirty;
}
