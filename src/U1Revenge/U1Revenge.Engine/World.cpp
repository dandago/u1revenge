#include <vector>
#include <fstream>
#include <memory>

#include "World.h"
#include "Castle.h"
#include "Town.h"
#include "SignPost.h"
#include "Dungeon.h"

using std::ifstream;
using std::vector;

// private methods

void World::AddLocation(Location* location)
{
    int x = location->GetX();
    int y = location->GetY();

    int key = Location::GetLocationKey(x, y);

    locations[key] = location;
}

// public methods

World::World()
{
    // Lands of Lord British

    AddLocation(new Castle("The Castle of Lord British", 0x28, 0x26, 0));
    AddLocation(new Castle("The Castle of the Lost King", 0x20, 0x1B, 1));

    AddLocation(new Town("Britian", 0x27, 0x27, 2));
    AddLocation(new Town("Moon", 0x42, 0x29, 3));
    AddLocation(new Town("Fawn", 0x19, 0x3D, 4));
    AddLocation(new Town("Paws", 0x2E, 0x1C, 5));
    AddLocation(new Town("Montor", 0x34, 0x3F, 6));
    AddLocation(new Town("Yew", 0x12, 0x22, 7));
    AddLocation(new Town("Tune", 0x46, 0x3F, 8));
    AddLocation(new Town("Grey", 0x40, 0x16, 9));

    AddLocation(new SignPost("The Pillars of Protection", 0x24, 0x09));
    AddLocation(new SignPost("The Tower of Knowledge", 0x45, 0x0A));

    AddLocation(new Dungeon("The Dungeon of Perinia", 0x12, 0x0D));
    AddLocation(new Dungeon("The Unholy Hole", 0x30, 0x0B));
    AddLocation(new Dungeon("The Dungeon of Montor", 0x35, 0x16));
    AddLocation(new Dungeon("The Mines of Mt. Drash", 0x3B, 0x1D));
    AddLocation(new Dungeon("Mondain's Gate to Hell", 0x1D, 0x25));
    AddLocation(new Dungeon("The Lost Caverns", 0x0D, 0x2B));
    AddLocation(new Dungeon("The Dungeon of Doubt", 0x3E, 0x31));
    AddLocation(new Dungeon("The Mines of Mt. Drash II", 0x27, 0x3C));
    AddLocation(new Dungeon("Death's Awakening", 0x26, 0x44));

    // Lands of the Feudal Lords

    AddLocation(new Castle("The Castle Barataria", 0x7D, 0x25, 0));
    AddLocation(new Castle("The Castle Rondorin", 0x72, 0x1D, 1));

    AddLocation(new Town("Arnold", 0x7E, 0x24, 2));
    AddLocation(new Town("Linda", 0x80, 0x3F, 3));
    AddLocation(new Town("Helen", 0x94, 0x16, 4));
    AddLocation(new Town("Owen", 0x73, 0x2B, 5));
    AddLocation(new Town("John", 0x96, 0x31, 6));
    AddLocation(new Town("Gerry", 0x79, 0x0F, 7));
    AddLocation(new Town("Wolf", 0x96, 0x43, 8));
    AddLocation(new Town("The Snake", 0x6D, 0x3D, 9));

    AddLocation(new SignPost("The Pillars of the Argonauts", 0x60, 0x21));
    AddLocation(new SignPost("The Pillar of Ozymandias", 0x61, 0x42));

    AddLocation(new Dungeon("The Savage Place", 0x82, 0x0A));
    AddLocation(new Dungeon("Scorpion Hole", 0x64, 0x0F));
    AddLocation(new Dungeon("Advari's Hole", 0x7C, 0x1A));
    AddLocation(new Dungeon("The Dead Warrior's Fight", 0x9B, 0x23));
    AddLocation(new Dungeon("The Horror of the Harpies", 0x93, 0x24));
    AddLocation(new Dungeon("The Labyrinth", 0x62, 0x2D));
    AddLocation(new Dungeon("Where Hercules Died", 0x6D, 0x32));
    AddLocation(new Dungeon("The Horror of the Harpies II", 0x74, 0x38));
    AddLocation(new Dungeon("The Gorgon Hole", 0x88, 0x3B));

    // Lands of Danger and Despair

    AddLocation(new Castle("The White Dragon's Castle", 0x7F, 0x74, 0));
    AddLocation(new Castle("The Castle of Shamino", 0x87, 0x69, 1));

    AddLocation(new Town("Gorlab", 0x80, 0x75, 2));
    AddLocation(new Town("Dextron", 0x65, 0x77, 3));
    AddLocation(new Town("Magic", 0x8E, 0x8B, 4));
    AddLocation(new Town("Wheeler", 0x79, 0x6A, 5));
    AddLocation(new Town("Bulldozer", 0x73, 0x8D, 6));
    AddLocation(new Town("The Brother", 0x95, 0x70, 7));
    AddLocation(new Town("Turtle", 0x61, 0x8D, 8));
    AddLocation(new Town("Lost Friends", 0x67, 0x64, 9));

    AddLocation(new SignPost("The Eastern Sign Post", 0x83, 0x57));
    AddLocation(new SignPost("The Grave of the Lost Soul", 0x62, 0x58));

    AddLocation(new Dungeon("The Skull Smasher", 0x77, 0x59));
    AddLocation(new Dungeon("The Spine Breaker", 0x95, 0x5B));
    AddLocation(new Dungeon("The Dungeon of Doom", 0x72, 0x64));
    AddLocation(new Dungeon("The Dead Cat's Life", 0x6C, 0x6B));
    AddLocation(new Dungeon("The Morbid Adventure", 0x8A, 0x73));
    AddLocation(new Dungeon("Free Death Hole", 0x9A, 0x79));
    AddLocation(new Dungeon("Dead Man's Walk", 0x69, 0x7F));
    AddLocation(new Dungeon("The Dead Cat's Life II", 0x80, 0x8A));
    AddLocation(new Dungeon("The Hole to Hades", 0x81, 0x92));

    // Lands of the Dark Unknown

    AddLocation(new Castle("The Castle of Olympus", 0x29, 0x76, 0));
    AddLocation(new Castle("The Black Dragon's Castle", 0x1E, 0x7E, 1));

    AddLocation(new Town("Nassau", 0x2A, 0x77, 2));
    AddLocation(new Town("Clear Lagoon", 0x2C, 0x5C, 3));
    AddLocation(new Town("Stout", 0x40, 0x85, 4));
    AddLocation(new Town("Gauntlet", 0x1F, 0x70, 5));
    AddLocation(new Town("Imagination", 0x42, 0x6A, 6));
    AddLocation(new Town("Ponder", 0x25, 0x8C, 7));
    AddLocation(new Town("Wealth", 0x42, 0x58, 8));
    AddLocation(new Town("Poor", 0x19, 0x5E, 9));

    AddLocation(new SignPost("The Sign Post", 0x0D, 0x59));
    AddLocation(new SignPost("The Southern Sign Post", 0x0C, 0x7A));

    AddLocation(new Dungeon("The Tramp of Doom", 0x34, 0x60));
    AddLocation(new Dungeon("The Viper's Pit", 0x20, 0x63));
    AddLocation(new Dungeon("The Long Death", 0x19, 0x69));
    AddLocation(new Dungeon("The End...", 0x0E, 0x6E));
    AddLocation(new Dungeon("The Viper's Pit II", 0x3F, 0x77));
    AddLocation(new Dungeon("The Slow Death", 0x47, 0x78));
    AddLocation(new Dungeon("The Guild of Death", 0x28, 0x81));
    AddLocation(new Dungeon("The Metal Twister", 0x10, 0x8C));
    AddLocation(new Dungeon("The Troll's Hole", 0x2E, 0x91));
}

World::~World()
{
    delete[] cells;

    for (auto location : locations)
        delete location.second;
}

void World::Load(const char * filePath)
{
    const int fileSize = 13104;
    const int worldWidth = 168;
    const int worldHeight = 156;

    ifstream file(filePath, std::ios::binary | std::ios::in);
    vector<char> buffer(fileSize);
    cells = new char[worldWidth * worldHeight];
    int cellIndex = 0;

    if (file.read(buffer.data(), fileSize))
    {
        for (int i = 0; i < fileSize; i++) // goes through each byte
        {
            char currentByte = buffer[i];
            char tileIndex = 0; // this is the value at the spot we're reading, used to index a tile

            for (int j = 0; j < 2; j++) // determines whether to take high or low half-byte
            {
                if (j == 0)
                    tileIndex = (currentByte & 0xF0) >> 4;
                else
                    tileIndex = currentByte & 0x0F;

                cells[cellIndex] = tileIndex;
                cellIndex++;
            }
        }
    }
    else
    {
        // TODO handle error case
    }

    file.close();
}

char World::GetTileIndexAt(int x, int y) const
{
    return cells[y * 168 + x];
}

const char* World::GetLocationName(int x, int y) const
{
    char tileIndex = GetTileIndexAt(x, y);

    switch (tileIndex)
    {
        case 0: // water
            return "You are at sea";
        case 1: // grass
            if (y < 156 / 2) // top hemisphere
            {
                if (x < 168 / 2) // top-left quadrant
                    return "You are in the lands\nof Lord British";
                else // top-right quadrant
                    return "You are in the lands\nof the Feudal Lords";
            }
            else // bottom hemisphere
            {
                if (x < 168 / 2) // bottom-left quadrant
                    return "You are in the lands\nof the Dark Unknown";
                else // bottom-right quadrant
                    return "You are in the lands\nof Danger and Despair";
            }
            break;
        case 2: // woods
            return "You are in the woods";
        case 3: // mountains - this will no longer be valid once collision detection is implemented
            return "Mountains are impassable!";
        default:
        {
            int key = Location::GetLocationKey(x, y);

            auto iterator = locations.find(key);

            if (iterator == locations.end()) // not found
                return "Location not found!"; // should not happen
            else
                return iterator->second->GetFullName();
            break;
        }
    }
}
