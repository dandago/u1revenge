#ifndef _LOCATION_H_
#define _LOCATION_H_

#include <string>

class Location
{
private:
    const char* name;
    std::string fullName;
    int x;
    int y;
public:
    Location(const char* name, const char* prefix, int x, int y);
    ~Location();
    virtual const char* GetName();
    virtual const char* GetFullName();
    int GetX() const;
    int GetY() const;
    static int GetLocationKey(int x, int y);
};

#endif