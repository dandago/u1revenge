#ifndef _EGA_TILESET_H_
#define _EGA_TILESET_H_

#include "Tileset.h"

class EgaTileset : public Tileset
{
private:
    int ComputeColour(int r8, int g8, int b8, int a8, int offset);
public:
    EgaTileset();
    ~EgaTileset();
    virtual void Load(const char* filePath);
};

#endif