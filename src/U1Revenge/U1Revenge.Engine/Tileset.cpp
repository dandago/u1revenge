#include <fstream>
#include <vector>

#include "Palette.h"
#include "Tileset.h"
#include "ErrorOut.h"

Tileset::Tileset()
{
    pixels = new int[13312]; // 52 tiles x 16x16 pixels
}

Tileset::~Tileset()
{
    delete[] pixels;
}

int * Tileset::GetPixels(char tileIndex) const
{
    // here we get a reference to the first pixel in the requested tile

    return pixels + (tileWidth * tileHeight * tileIndex);
}
