#ifndef _WEAPON_H_
#define _WEAPON_H_

enum Weapon
{
    Hands = 0,
    Dagger = 1,
    Mace = 2,
    Axe = 3,
    RopeAndSpikes = 4,
    Sword = 5,
    GreatSword = 6,
    BowAndArrows = 7,
    Amulet = 8,
    Wand = 9,
    Staff = 10,
    Triangle = 11,
    Pistol = 12,
    LightSword = 13,
    Phazor = 14,
    Blaster = 15
};

#endif