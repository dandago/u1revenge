#ifndef _SOUNDEFFECTPLAYER_H_
#define _SOUNDEFFECTPLAYER_H_

#include <SDL.h>

#include "SoundEffect.h"

class SoundEffectPlayer
{
private:
	SDL_AudioDeviceID deviceId;
	SoundEffect moveGrassSound;

	void PlaySoundEffect(SoundEffect& soundEffect);
public:
	SoundEffectPlayer();
	~SoundEffectPlayer();
	void PlayMoveGrassSound();
};

#endif