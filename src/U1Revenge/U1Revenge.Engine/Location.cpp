#include "Location.h"

using std::string;

Location::Location(const char* name, const char* prefix, int x, int y)
    : name(name), x(x), y(y)
{
    if (prefix != nullptr)
        fullName.append(prefix);
    fullName.append(name);
}

Location::~Location()
{
}

const char * Location::GetName()
{
    return name;
}

const char* Location::GetFullName()
{
    return fullName.c_str();
}

int Location::GetX() const
{
    return x;
}

int Location::GetY() const
{
    return y;
}

int Location::GetLocationKey(int x, int y)
{
    int key = y * 168 + x;
    return key;
}