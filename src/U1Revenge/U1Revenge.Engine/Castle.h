#ifndef _CASTLE_H_
#define _CASTLE_H_

#include "Location.h"

class Castle : public Location
{
private:
    int type;
public:
    Castle(const char* name, int type, int x, int y);
    ~Castle();
    int GetType();
};

#endif