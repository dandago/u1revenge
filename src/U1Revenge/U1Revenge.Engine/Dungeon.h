#ifndef _DUNGEON_H_
#define _DUNGEON_H_

#include "Location.h"

class Dungeon : public Location
{
public:
    Dungeon(const char* name, int x, int y);
    ~Dungeon();
};

#endif