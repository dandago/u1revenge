#include "GameEngine.h"
#include "ErrorOut.h"
#include "TilesetFactory.h"

// On Linux and macOS the font path is set through the Makefile
#ifdef FONTPATH
#define _FONTPATH FONTPATH
#else
#define _FONTPATH
#endif

#define stringify( x ) stringify_literal( x )
#define stringify_literal( x ) # x

// public methods

GameEngine::GameEngine(Sdl2Core* sdl2Core, int width, int height, int scale, string u1Path,
    MessageLog* messageLog, World* world, int viewportWidth, int viewportHeight,
    Player* player, GameSettings& gameSettings)
    : sdl2Core(sdl2Core), width(width), height(height), gameSettings(gameSettings),
		inputController(nullptr)
{
    string graphics = gameSettings.Get("Graphics", "T1K");

	auto renderer = sdl2Core->GetRenderer();

    font = TTF_OpenFont(stringify( _FONTPATH ) "pcsenior.ttf", 16);
    if (!font)
        ErrorOut("Could not load Font file " stringify( _FONTPATH ) "pcsenior.ttf");

    std::string tilesetFilePath = u1Path + '/' + graphics + "TILES.BIN"; // e.g. T1KTILES.BIN

    TilesetFactory tilesetFactory;
    tileset = tilesetFactory.CreateTileset(graphics);
    if (!tileset)
        ErrorOut("Unrecognised graphics format: " + graphics);

    tileset->Load(tilesetFilePath.c_str());

	inputController = new SosariaInputController(sdl2Core, player, world, messageLog, tileset);

	int* iconPixels = tileset->GetPixels(48);
    sdl2Core->SetWindowIcon(iconPixels);

    // create views

    borderView = new BorderView(renderer, width, height, scale);
    messageLogView = new MessageLogView(messageLog, renderer, font);
    bottomRightView = new BottomRightView(renderer, font, player, scale);
    viewportView = new ViewportView(renderer, width, height, viewportWidth, viewportHeight,
        world, tileset, player);
}

GameEngine::~GameEngine()
{
    delete viewportView;
    delete bottomRightView;
    delete messageLogView;
    delete borderView;
    delete tileset;

    TTF_CloseFont(font);
}

void GameEngine::Render() const
{
	auto renderer = sdl2Core->GetRenderer();

    // clear all to black

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // black
    SDL_RenderClear(renderer);

    // render viewport, message log and bottom right

    viewportView->Render();
    messageLogView->Render();
    bottomRightView->Render();

    // render borders - note these don't scale at the moment

    borderView->Render();

    // swap video buffers

    SDL_RenderPresent(renderer);
}

bool GameEngine::ProcessInputEvent(SDL_Event& event)
{
	return inputController->ProcessInputEvent(event);
}