#include <fstream>
#include <vector>

#include "CgaTileset.h"
#include "ErrorOut.h"

CgaTileset::CgaTileset()
{
}

CgaTileset::~CgaTileset()
{
}

void CgaTileset::Load(const char* filePath)
{
    const int fileSize = 3328;
    std::ifstream file(filePath, std::ios::binary | std::ios::in);
    std::vector<char> buffer(fileSize);

    int pixelIndex = 0;

    if (file.read(&buffer[0], fileSize))
    {
        // convert file bytes to pixel colour array

        for (int i = 0; i < fileSize; i++) // goes through each byte
        {
            char currentByte = buffer[i];
            char paletteIndex = 0; // this is the pixel value, to be translated to colour later

            for (int j = 0; j < 4; j++) // determines which 2 bits to take from the byte
            {
                switch (j)
                {
                    case 0: paletteIndex = (currentByte >> 6) & 0x03; break;
                    case 1: paletteIndex = (currentByte >> 4) & 0x03; break;
                    case 2: paletteIndex = (currentByte >> 2) & 0x03; break;
                    case 3: paletteIndex = (currentByte >> 0) & 0x03; break;
                }

                int pixelColour = palette.IndexToColour(paletteIndex);
                pixels[pixelIndex] = pixelColour;
                pixelIndex++;
            }
        }
    }
    else
    {
        ErrorOut("Unable to open Ultima 1 tileset " << filePath);
    }

    file.close();
}