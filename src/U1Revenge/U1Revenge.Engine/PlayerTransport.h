#ifndef _PLAYERTRANSPORT_H_
#define _PLAYERTRANSPORT_H_

enum PlayerTransport
{
    Foot = 10,
    Horse = 11,
    Cart = 12,
    Raft = 13,
    Frigate = 14,
    Aircar = 16,
    Shuttle = 17,
    TimeMachine = 18
};

#endif