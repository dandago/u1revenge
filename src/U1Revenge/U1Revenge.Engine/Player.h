#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "PlayerTransport.h"
#include "Weapon.h"
#include "Armour.h"
#include "MessageLog.h"

class Player
{
private:
    int x; // player location
    int y; // player location
    int moveCount;
    PlayerTransport transport;
    Weapon weapon;
    Armour armour;
    unsigned short hits;
    unsigned short food;
    unsigned short experience;
    unsigned short coin;
	bool hitsDirty;
	bool foodDirty;
	bool experienceDirty;
	bool coinDirty;

    void IncrementMoveCount();
public:
    Player(int x, int y);
    ~Player();
    PlayerTransport GetTransport() const;
    int GetX() const;
    int GetY() const;
    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();
    void Pass();
    void CycleTransportLeft();
    void CycleTransportRight();
    unsigned short GetHits() const;
    unsigned short GetFood() const;
    unsigned short GetExperience() const;
    unsigned short GetCoin() const;
	bool GetAndRemoveHitsDirty();
	bool GetAndRemoveFoodDirty();
	bool GetAndRemoveExperienceDirty();
	bool GetAndRemoveCoinDirty();
};

#endif