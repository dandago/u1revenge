#include <fstream>
#include <vector>

#include "EgaTileset.h"
#include "ErrorOut.h"

// private methods

int EgaTileset::ComputeColour(int r8, int g8, int b8, int a8, int offset)
{
    int r = (r8 >> offset) & 0x01;
    int g = (g8 >> offset) & 0x01;
    int b = (b8 >> offset) & 0x01;
    int a = (a8 >> offset) & 0x01;

    int m = (a == 1) ? 0xFF : 0x7F; // a: 1 => bright; 0 => dim

    r *= m;
    g *= m;
    b *= m;

    int colour = 0xFF000000;
    colour += (r << 16);
    colour += (g << 8);
    colour += b;

    return colour;
}

// public methods

EgaTileset::EgaTileset()
{
}

EgaTileset::~EgaTileset()
{
}

void EgaTileset::Load(const char* filePath)
{
    const int fileSize = 6656;
    const int tileCount = 52;
    const int bytesPerTile = 128;
    std::ifstream file(filePath, std::ios::binary | std::ios::in);
    std::vector<char> buffer(fileSize);

    if (file.read(&buffer[0], fileSize))
    {
        // this implementation will need to be revised when we implement town tilesets

        int* currentPixel = pixels;

        for (int tileNo = 0; tileNo < tileCount; tileNo++)
        {
            int offsetBase = tileNo * bytesPerTile;

            for (int i = 0; i < 16; i++)
            {
                int iterationOffset = offsetBase + (i * 8);

                // |                       16 pixels / 1 row                      |
                // | Blue  16 bits|| Green 16 bits|| Red   16 bits|| Alpha 16 bits|
                // BBBBBBBBBBBBBBBBGGGGGGGGGGGGGGGGRRRRRRRRRRRRRRRRAAAAAAAAAAAAAAAA
                //                |               |               |               |
                //                +---------------+---------------+---------------+
                //                |
                //                first pixel = (R15 * A15, G15 * A15, B15 * A15)

                int b8 = ((unsigned char)buffer[iterationOffset + 0] << 8) | ((unsigned char)buffer[iterationOffset + 1]);
                int g8 = ((unsigned char)buffer[iterationOffset + 2] << 8) | ((unsigned char)buffer[iterationOffset + 3]);
                int r8 = ((unsigned char)buffer[iterationOffset + 4] << 8) | ((unsigned char)buffer[iterationOffset + 5]);
                int a8 = ((unsigned char)buffer[iterationOffset + 6] << 8) | ((unsigned char)buffer[iterationOffset + 7]);

                for (int j = 0; j < 16; j++)
                {
                    *currentPixel = ComputeColour(r8, g8, b8, a8, 16 - j - 1);
                    currentPixel++;
                }
            }
        }
    }
    else
    {
        ErrorOut("Unable to open Ultima 1 tileset " << filePath);
    }

    file.close();
}