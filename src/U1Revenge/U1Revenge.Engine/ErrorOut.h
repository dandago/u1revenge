#ifndef _ERROROUT_H_
#define _ERROROUT_H_

#include <iostream>
#include <cstdlib>

#define ErrorOut(msg)                                                             \
    do                                                                            \
    {                                                                             \
        std::cerr << msg << std::endl;                                            \
        std::exit(1);                                                             \
    } while (0)

#endif
