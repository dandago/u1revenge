#ifndef _PALETTE_H_
#define _PALETTE_H_

class Palette
{
public:
    Palette();
    ~Palette();
    virtual int IndexToColour(char index) = 0;
};

#endif