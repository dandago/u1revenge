#include "T1kPalette.h"

T1kPalette::T1kPalette()
{
}

T1kPalette::~T1kPalette()
{
}

int T1kPalette::IndexToColour(char index)
{
    switch (index)
    {
        case 0: return 0xFF000000;
        case 1: return 0xFF000080;
        case 2: return 0xFF008000;
        case 3: return 0xFF008080;
        case 4: return 0xFF800000;
        case 5: return 0xFF800080;
        case 6: return 0xFF808000;
        case 7: return 0xFFC0C0C0;
        case 8: return 0xFF808080;
        case 9: return 0xFF0000FF;
        case 10: return 0xFF00FF00;
        case 11: return 0xFF00FFFF;
        case 12: return 0xFFFF4040;
        case 13: return 0xFFFF00FF;
        case 14: return 0xFFFFFF00;
        case 15: return 0xFFFFFFFF;
        default: return 0xFF000000;
    }
}