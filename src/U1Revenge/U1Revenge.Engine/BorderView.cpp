#include "BorderView.h"

BorderView::BorderView(SDL_Renderer* renderer, int width, int height, int scale)
    : View(renderer), width(width), height(height), scale(scale)
{
}

BorderView::~BorderView()
{
}

void BorderView::Render()
{
    auto renderer = GetRenderer();

    // render borders - note these don't scale at the moment

    SDL_SetRenderDrawColor(renderer, 0, 0, 176, 255); // blue

    SDL_RenderFillRect(renderer, &topBorder);
    SDL_RenderFillRect(renderer, &leftBorder);
    SDL_RenderFillRect(renderer, &rightBorder);
    SDL_RenderFillRect(renderer, &bottomBorder);
    SDL_RenderFillRect(renderer, &bottomSideBorder);

    // render white lines over blue borders

    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // white

    SDL_RenderFillRect(renderer, &viewportTopHorizontalWhite);
    SDL_RenderFillRect(renderer, &viewportLeftVerticalWhite);
    SDL_RenderFillRect(renderer, &viewportRightVerticalWhite);
    SDL_RenderFillRect(renderer, &viewportBottomHorizontalWhite);

    SDL_RenderFillRect(renderer, &messageLogTopWhite);
    SDL_RenderFillRect(renderer, &messageLogRightWhite);
    SDL_RenderFillRect(renderer, &playerStatsLeftWhite);
    SDL_RenderFillRect(renderer, &playerStatsTopWhite);
}
