#ifndef _SOUNDEFFECT_H_
#define _SOUNDEFFECT_H_

#include <SDL.h>

class SoundEffect
{
private:
	const char* filename;
	SDL_AudioSpec wavSpec;
	Uint8 *wavBuffer;
	Uint32 wavLength;

public:
	SoundEffect(const char* filename);
	~SoundEffect();

	SDL_AudioSpec GetWavSpec();
	Uint8 *GetWavBuffer();
	Uint32 GetWavLength();
};

#endif